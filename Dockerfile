FROM docker:latest

ENV PATH /root/.gem/ruby/2.3.0/bin:$PATH

RUN apk add --no-cache git python3 && pip3 install docker-compose
